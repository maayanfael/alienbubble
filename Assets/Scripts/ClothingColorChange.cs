﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothingColorChange : MonoBehaviour
{
    public void ChangeColor(Color color)
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        // ... set their material color to the color specific to this tank.
        renderer.color = color;
    }
}
