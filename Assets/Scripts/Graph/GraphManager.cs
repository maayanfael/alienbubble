﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GraphManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        /*
        MyGraph myGraph = new MyGraph();
        for (int i = 1; i <= 6; i++)
        {
            myGraph.AddNode(i);
        }

        myGraph.AddEdge(1, 2);
        myGraph.AddEdge(1, 5);
        myGraph.AddEdge(2, 3);
        myGraph.AddEdge(2, 5);
        myGraph.AddEdge(3, 4);
        myGraph.AddEdge(4, 5);
        myGraph.AddEdge(4, 6);

        Debug.Log(myGraph);

        MyNode start = myGraph.GetNodeById(1);
        MyNode end = myGraph.GetNodeById(6);
        List<MyNode> path = myGraph.CalculateDFS(start, end);
        if (path == null)
        {
            Debug.Log("Failed to find path");
        }
        else
        {            
            string pathStr = "Path Found: ";
            foreach (MyNode node in path)
            {
                pathStr += " " + node.id;
            }
            Debug.Log(pathStr);

        }
        */
        Astar();
    }

    private void Astar()
    {
        AGraph myGraph = new AGraph();
        for (int i = 1; i <= 6; i++)
        {

            myGraph.AddNode(new Vector3(
                Random.Range(-5, 6), Random.Range(-5, 6), Random.Range(-5, 6))
                , i);
        }

        myGraph.AddEdge(1, 2);
        myGraph.AddEdge(1, 5);
        myGraph.AddEdge(2, 3);
        myGraph.AddEdge(2, 5);
        myGraph.AddEdge(3, 4);
        myGraph.AddEdge(4, 5);
        myGraph.AddEdge(4, 6);

        Debug.Log(myGraph);
        ANode start = myGraph.GetNodeById(1);
        ANode end = myGraph.GetNodeById(6);
        List<ANode> foundPath =
            myGraph.CalculateAStar(start, end);
        if (foundPath == null)
        {
            Debug.Log("No path...");
        }
        else
        {
            Debug.Log("Found path: ");
            myGraph.PrintPath();
        }
    }
}