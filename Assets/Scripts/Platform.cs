﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    private const float maxTimeToWait = 0.5f;
    private PlatformEffector2D effector;
    private float waitTime; 

    // Start is called before the first frame update
    void Start()
    {
        effector = GetComponent<PlatformEffector2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.DownArrow)){
            waitTime = maxTimeToWait;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            if(waitTime <= 0)
            {
                effector.rotationalOffset = 180f;
                waitTime = maxTimeToWait;

                Invoke("PlatformRotation", 0.3f);
            }
            else
            {
                waitTime -= Time.deltaTime;
            }
            
        }
        
    }

    void PlatformRotation()
    {
        effector.rotationalOffset = 0;
    }
}
