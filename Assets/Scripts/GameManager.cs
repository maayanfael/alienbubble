﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject m_WinPanel;
    public GameObject m_LostPanel;
    public GameObject m_PlayerPrefab;
    public GameObject[] m_EnemyPrefabs;
    public CharacterManager[] m_Players;
    public GameObject[] m_Enemies;
    public float m_EndDelay = 3f;              

    private WaitForSeconds m_EndWait;          

    // Start is called before the first frame update
    void Start()
    {
        m_EndWait = new WaitForSeconds(m_EndDelay);

        SpawnAllPlayers();
        GetAllEnemies();

        StartCoroutine(GameLoop());

    }

    // This is called from start and will run each phase of the game one after another.
    private IEnumerator GameLoop()
    {
        // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundPlaying());

        // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
        yield return StartCoroutine(RoundEnding());

        // If there is a game winner, restart the level.
        SceneManager.LoadScene(0);
    }

    private void SpawnAllPlayers()
    {
        // For all the tanks...
        for (int i = 0; i < m_Players.Length; i++)
        {
            // ... create them, set their player number and references needed for control.
            m_Players[i].m_Instance =
                Instantiate(m_PlayerPrefab, m_Players[i].m_SpawnPoint.position, m_Players[i].m_SpawnPoint.rotation) as GameObject;
            m_Players[i].m_PlayerNumber = i + 1;
            m_Players[i].Setup();
        }
    }

    private void GetAllEnemies()
    {
        m_Enemies = GameObject.FindGameObjectsWithTag("Enemy");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private IEnumerator RoundPlaying()
    {
        // While there is not one tank left...
        while (!NoPlayersLeft() && !NoEnemiesLeft())
        {
            // ... return on the next frame.
            yield return null;
        }
    }

    private bool NoPlayersLeft()
    {
        // Start the count of tanks left at zero.
        int numPlayersLeft = 0;

        // Go through all the tanks...
        for (int i = 0; i < m_Players.Length; i++)
        {
            // ... and if they are active, increment the counter.
            if (m_Players[i].m_Instance.activeSelf)
                numPlayersLeft++;
        }

        // If there are one or fewer tanks remaining return true, otherwise return false.
        return numPlayersLeft == 0;
    }


    private bool NoEnemiesLeft()
    {
        // Start the count of tanks left at zero.
        int numEnemiesLeft = 0;

        // Go through all the tanks...
        for (int i = 0; i < m_Enemies.Length; i++)
        {
            // ... and if they are active, increment the counter.
            if (m_Enemies[i])
                numEnemiesLeft++;
        }

        // If there are one or fewer tanks remaining return true, otherwise return false.
        return numEnemiesLeft == 0;
    }

    private IEnumerator RoundEnding()
    {

        if (NoEnemiesLeft())
        {
            m_WinPanel.SetActive(true);
        }
        else
        {
            m_LostPanel.SetActive(true);
        }


        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_EndWait;
    }

}
