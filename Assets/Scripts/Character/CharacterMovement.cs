﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    public int m_PlayerNumber = 1;
    public float m_RunSpeed = 40f;


    private CharacterController2D m_Controller;
    private Animator m_Animator;
    private float m_HorizontalMove = 0f;
    private bool m_IsJumping = false;


    private void Start()
    {
        m_Controller = GetComponent<CharacterController2D>();
        m_Animator = GetComponentInChildren<Animator>();


    }
    // Update is called once per frame
    void Update()
    {
        m_HorizontalMove = Input.GetAxisRaw("Horizontal" + m_PlayerNumber) * m_RunSpeed;
        
        if (!m_IsJumping)
        {
            m_Animator.SetFloat("Speed", Mathf.Abs(m_HorizontalMove));
        }
        if (Input.GetButtonDown("Jump"+ m_PlayerNumber))
        {
            m_IsJumping = true;
            m_Animator.SetBool("IsJumping", true);
        }
        
    }

    void FixedUpdate()
    {
        // Move our character
        m_Controller.Move(m_HorizontalMove * Time.fixedDeltaTime, false, m_IsJumping);
        m_IsJumping = false;
    }

    public void OnLanding()
    {
        m_Animator.SetBool("IsJumping", false);
    }
}