﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterHealth : MonoBehaviour
{

    [HideInInspector] public bool m_LivesChanged = false;
    [HideInInspector] public int m_Lives = 5;

    public int m_PlayerNumber = 1;              

    private Renderer[] m_Renderers;
    private bool m_DuringBlinking = false;
    public System.Action<int> onScoreChange;
    public System.Action<int> onLivesChange;

    public void Setup()
    {
        m_Renderers = GetComponentsInChildren<Renderer>(true);
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bubble"))
        {
            BubbleManager Bubble = other.GetComponent<BubbleManager>();
            if (!Bubble.CanCatchSomething())
            {
                if (onScoreChange != null) onScoreChange.Invoke(Bubble.m_Worth);

                Destroy(other.gameObject);

            }
            else if (Bubble.CanCatchSomething() && Bubble.m_Shooter != m_PlayerNumber)
            {
                if (onScoreChange != null) onScoreChange.Invoke(Bubble.m_Worth);
                Destroy(other.gameObject);

            }
        }
        else if (other.CompareTag("Enemy") && !m_DuringBlinking)
        {
            m_Lives--;

            if (onLivesChange != null) onLivesChange.Invoke(m_Lives);
            
            if (m_Lives < 1)
            {
                Die();
            }
            else
            {
                StartCoroutine(Blink(4f, 0.3f));
            }
        }
    }

    IEnumerator Blink(float duration, float blinkTime)
    {
        m_DuringBlinking = true;

        while (duration > 0f)
        {
            duration -= 0.5f;

            // Toggle renderer
            foreach (var renderer in m_Renderers)
            {
                renderer.enabled = !renderer.enabled;
            }

            // Wait for a bit
            yield return new WaitForSeconds(blinkTime);
        }

        // Make sure renderer is enabled when we exit
        foreach (var renderer in m_Renderers)
        {
            renderer.enabled = true;
        }

        m_DuringBlinking = false;
    }



    void Die()
    {
        gameObject.SetActive(false);
        //Destroy(m_Instance);
    }
}
