﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using TMPro;
using System;

[Serializable]
public class CharacterManager
{
    public Color m_PlayerColor;
    public int m_PlayerNumber = 1;
    public Transform m_SpawnPoint;
    public TextMeshProUGUI m_ScoreGUI;
    public TextMeshProUGUI m_LivesGUI;
    public TextMeshProUGUI m_NameGUI;
    [HideInInspector] public GameObject m_Instance;
    [HideInInspector] public int m_Score;

    private CharacterMovement m_Movement;
    private CharacterShooter m_Shooting;
    private CharacterHealth m_Health;


    internal void Setup()
    {
        m_Movement = m_Instance.GetComponent<CharacterMovement>();
        m_Shooting = m_Instance.GetComponent<CharacterShooter>();
        m_Health = m_Instance.GetComponent<CharacterHealth>();

        m_Health.Setup();

        // Setting GUI text
        m_ScoreGUI.text = m_Score.ToString();
        m_LivesGUI.text = m_Health.m_Lives.ToString();
        m_NameGUI.text = "Player " + m_PlayerNumber;

        m_Movement.m_PlayerNumber = m_PlayerNumber;
        m_Shooting.m_PlayerNumber = m_PlayerNumber;
        m_Health.m_PlayerNumber = m_PlayerNumber;

        ClothingColorChange[] ClothingColorComponents = m_Instance.GetComponentsInChildren<ClothingColorChange>();

        for (int i = 0; i < ClothingColorComponents.Length; i++)
        {
            ClothingColorComponents[i].ChangeColor(m_PlayerColor);
        }

        m_Health.onLivesChange += UpdateLives;
        m_Health.onScoreChange += AddScore;

    }

    void UpdateLives(int Lives)
    {
        m_LivesGUI.text = Lives.ToString();
    }

    void AddScore(int value)
    {
        m_Score += value;
        m_ScoreGUI.text = m_Score.ToString();
    }
}


