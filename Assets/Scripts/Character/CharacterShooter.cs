﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterShooter : MonoBehaviour
{
    public int m_PlayerNumber = 1;              // Used to identify the different players.
    public Rigidbody2D m_Bubble;                  // Bubble of the shell.
    public Transform m_BlowTransform;         // A child of the player where the bubbles are spawned.
    public AudioClip m_FireClip;                // Audio that plays when each shot is fired.
    public float m_LaunchForce = 15f;        // The force given to the shell if the fire button is not held.
    
    private string m_BlowButton;                // The input axis that is used for launching shells.
    private bool m_Blowd;                       // Whether or not the shell has been launched with this button press.


    private void Start()
    {
        // The fire axis is based on the player number.
        m_BlowButton = "Fire" + m_PlayerNumber;
    }


    private void Update()
    {
        if (Input.GetButtonUp(m_BlowButton) && !m_Blowd)
        {
            // ... launch the bubble.
            Blow();
        }
    }


    private void Blow()
    {
        // Set the fired flag so only Fire is only called once.
        m_Blowd = true;

        // Create an instance of the shell and store a reference to it's rigidbody.
        Rigidbody2D bubbleInstance =
            Instantiate(m_Bubble, m_BlowTransform.position, m_BlowTransform.rotation) as Rigidbody2D;
        bubbleInstance.GetComponent<BubbleManager>().m_Shooter = m_PlayerNumber;
        // Set the shell's velocity to the launch force in the fire position's forward direction.
        bubbleInstance.velocity = m_LaunchForce * m_BlowTransform.up;

        m_Blowd = false;
    }
}
