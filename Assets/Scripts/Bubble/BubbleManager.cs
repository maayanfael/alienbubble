﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BubbleManager : MonoBehaviour
{
    public float m_PoppingTime = 15f;
    public float m_Speed = 5f;
    public int m_Worth = 10;
    public int m_Shooter;
    private float m_HittingTiming = 1f;
    private float m_CountTime;
    private bool m_CoughtSomething = false;


    // Start is called before the first frame update
    void Start()
    {
        m_CountTime = Time.realtimeSinceStartup;
        m_CoughtSomething = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.realtimeSinceStartup > m_CountTime + m_PoppingTime)
        {
            Destroy(gameObject);
        }
    }

    public bool CanCatchSomething()
    {
        bool timing = Time.realtimeSinceStartup < m_CountTime + m_HittingTiming;
        bool returnVal = timing & !m_CoughtSomething;
        
        return (returnVal);
    }

    public void ExtendPoppingTime(float time)
    {
        m_CountTime = Time.realtimeSinceStartup;
        m_PoppingTime = time;
    }

    public void UpdateWorth(int worth)
    {
        m_Worth += worth;
        m_CoughtSomething = true;
    }
}


