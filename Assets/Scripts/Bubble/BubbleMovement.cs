﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BubbleManager))]
public class BubbleMovement : MonoBehaviour
{

    private Transform m_Target;
    private int m_WaypointIndex = 0;
    private bool m_ControlMovement = false;
    private BubbleManager m_Bubble;

    void Start()
    {
        Invoke("ChangeBubbleMovement", 1f);
    }

    void Update()
    {
        if (m_ControlMovement)
        {
            Vector3 dir = m_Target.position - transform.position;
            transform.Translate(dir.normalized * m_Bubble.m_Speed * Time.deltaTime, Space.World);

            if (Vector3.Distance(transform.position, m_Target.position) <= 0.4f)
            {
                GetNextWaypoint();
            }
        }
    }

    void GetNextWaypoint()
    {
        m_WaypointIndex++;
        m_WaypointIndex = m_WaypointIndex % Waypoints.m_Points.Length;
        m_Target = Waypoints.m_Points[m_WaypointIndex];
    }

    int GetClosestWaypoint()
    {
        int closestIndex = 0;
        float closestDist = Vector3.Distance(transform.position, Waypoints.m_Points[0].position);

        for (int i = 1; i < Waypoints.m_Points.Length; i++)
        {
            float currentDist = Vector3.Distance(transform.position, Waypoints.m_Points[i].position);
            if (closestDist > currentDist)
            {
                closestDist = currentDist;
                closestIndex = i;
            }
        }

        return closestIndex;
    }


    void ChangeBubbleMovement()
    {
        m_Bubble = GetComponent<BubbleManager>();
        GetComponent<Rigidbody2D>().isKinematic = true;
        m_WaypointIndex = GetClosestWaypoint();
        m_Target = Waypoints.m_Points[m_WaypointIndex];
        GetComponent<Rigidbody2D>().velocity = Vector3.zero;
        m_ControlMovement = true;
    }
}
