﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float m_Speed = 10f;
    public bool m_IsMoveing = true;

    protected bool m_FacingRight = false;
    
    virtual public void StopMoving()
    {
        m_IsMoveing = false;

        // Change position to be under the bubble
        gameObject.transform.localPosition = Vector3.zero;
        gameObject.transform.eulerAngles = Vector3.zero;
        gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = 5;
    }

    virtual public void StartMoving()
    {
        m_IsMoveing = true;

        // Change position to be above the bubble
        gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = 7;

    }

    virtual protected void FaceRight()
    {
        transform.eulerAngles = new Vector3(0, -180, 0);
        m_FacingRight = true;
    }

    virtual protected void FaceLeft()
    {
        transform.eulerAngles = new Vector3(0, 0, 0);
        m_FacingRight = false;
    }
}
