﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpossumMovement : EnemyMovement
{
    public Transform m_GroundCheckPos;
    private bool m_IsGrounded;
    private bool m_IsFalling = false;
    private float m_RaycastDistance = 1f;
    

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D groundInfo = Physics2D.Raycast(m_GroundCheckPos.position, Vector2.down, m_RaycastDistance);

        if (m_IsFalling && !groundInfo.collider)
        {
            GetToGround();
        }else if (m_IsFalling && !groundInfo.collider.CompareTag("Bubble"))
        {
            OnGround(); 
        }
        
        if (m_IsMoveing && !m_IsFalling)
        {
            transform.Translate(Vector2.left * m_Speed * Time.deltaTime);

            if (!groundInfo.collider && !m_FacingRight)
            {
                FaceRight();
            }
            else if (!groundInfo.collider && m_FacingRight)
            {
                FaceLeft();
            }
        }
    }

    override public void StartMoving()
    {
        gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = 7;
        GetToGround();
        m_IsFalling = true;

    }

    private void GetToGround()
    {
        transform.Translate(Vector2.down * m_Speed/2 * Time.deltaTime);

    }

    private void OnGround()
    {
        m_IsMoveing = true;
        m_IsFalling = false;
        
    }
}
