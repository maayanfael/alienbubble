﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class EnemyManager : MonoBehaviour
{
    public int m_Worth = 40;
    public float m_TimeInBubble = 20f;
    [HideInInspector] public bool m_IsInsideBubble = false;

    private EnemyMovement m_EnemyMovement;

    private void Start()
    {
        m_EnemyMovement = GetComponent<EnemyMovement>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bubble") && !m_IsInsideBubble)
        {
            BubbleManager Bubble = other.GetComponent<BubbleManager>();
            if (Bubble.CanCatchSomething())
            {
                m_IsInsideBubble = true;

                // Change position of enemy to be under bubble
                GetComponent<Transform>().parent = other.GetComponent<Transform>();
                if(m_EnemyMovement == null)
                    m_EnemyMovement = gameObject.GetComponent<EnemyMovement>();

                // Change enemy's movement
                m_EnemyMovement.StopMoving();
                
                Bubble.ExtendPoppingTime(m_TimeInBubble);
                Bubble.UpdateWorth(m_Worth);
                Invoke("ReturnTolife", m_TimeInBubble - 0.5f);
            }
        }
    }

    private void ReturnTolife()
    {
        if (gameObject.activeInHierarchy)
        {
            GetComponent<Transform>().parent = null;
            m_EnemyMovement.StartMoving();
            m_IsInsideBubble = false;
        }
    }
}


