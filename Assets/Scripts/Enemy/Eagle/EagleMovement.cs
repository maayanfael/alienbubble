﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EagleMovement : EnemyMovement
{
    [SerializeField] private float m_NextWaypointDistance = 3f;
    private Transform m_Target;
    private Path m_Path;
    private int m_CurrentWaypoint = 0;
    private Seeker m_Seeker;
    private Rigidbody2D m_Rb;

    // Start is called before the first frame update
    void Start()
    {
        m_Seeker = GetComponent<Seeker>();
        m_Rb = GetComponent<Rigidbody2D>();

        InvokeRepeating("UpdatePath", 0f, .5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Path == null || !m_IsMoveing || m_CurrentWaypoint >= m_Path.vectorPath.Count)
        {
            return;
        }
        

        Vector2 direction = ((Vector2)m_Path.vectorPath[m_CurrentWaypoint] - m_Rb.position).normalized;
        transform.Translate(direction * m_Speed * Time.deltaTime, Space.World);

        if (Vector2.Distance(m_Rb.position, m_Path.vectorPath[m_CurrentWaypoint]) <= m_NextWaypointDistance)
        {
            m_CurrentWaypoint++;
        }

        if(direction.x >= 0.01f)
        {
            FaceRight();

        }
        else if (direction.x <= -0.01f)
        {
            FaceLeft();
        }
    }

    void FindTarget()
    {
        GameObject[] Go = GameObject.FindGameObjectsWithTag("Player");
        int closerIndex = -1;
        float dist = 9999;
        for (int i = 0; i < Go.Length; i++)
        {
            float currDist = Vector3.Distance(m_Rb.position, Go[i].transform.position);
            if (currDist < dist)
            {

                dist = currDist;
                closerIndex = i;
            }
        }
        if (closerIndex >= 0)
        {
            m_Target = Go[closerIndex].transform;

        }

    }

    void UpdatePath()
    {
        
        //if (m_Target==null || !m_Target.gameObject.activeSelf)
        //{
            FindTarget();
        //}
        if (m_Seeker.IsDone() && m_IsMoveing && (m_Target != null))
        {
            m_Seeker.StartPath(m_Rb.position, m_Target.position, OnPathComplete);
        }
    }

    void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            m_Path = p;
            m_CurrentWaypoint = 0;
        }
    }
}
